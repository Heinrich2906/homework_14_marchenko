package com.company;

/**
 * Created by heinr on 13.11.2016.
 */
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

/*
* Размытие изображения
* */
public class ImageBlurWithExecutorService {
    private static int imgSize;
    //Число пикселей в маске (13 - значит справа и слева по 6)
    private static int mBlurWidth = 13;

    public static void main(String[] args) {
        try {
            System.out.println("Loading image...");

            BufferedImage lImg = ImageIO.read(new File("cat.jpg"));
            System.out.println("Image loaded.");

            imgSize = lImg.getWidth();

            System.out.println("Processing image...");
            BufferedImage rImg = process(lImg);
            System.out.println("Image processed");

            System.out.println("Saving image...");
            ImageIO.write(rImg, "jpg", new File("out.jpg"));
            System.out.println("Image saved");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static BufferedImage process(BufferedImage lImg) throws ExecutionException, InterruptedException {
        //представляем изображение, как массив пикселей ()
        int[] rgb = imgToRgb(lImg);

        long start = System.currentTimeMillis();
        //задаём число потоков и задач
        int[] transformed = blurParallel(rgb, 2, 100);
        long end = System.currentTimeMillis();

        System.out.println((end - start) + "ms");

        //переводим массив пикселей в изображение
        return rgbToImg(transformed);
    }


    private static int[] blurParallel(int[] rgb, int threadsCount, int tasksCount) throws ExecutionException, InterruptedException {

        int[] res = new int[rgb.length];

        // преобразую линейный массив в двумерный
        int vertical = rgb.length / imgSize;
        int[][] square = new int[imgSize][vertical];

        for (int i = 0; i < imgSize; i++) {
            for (int j = 0; j < vertical; j++) {
                square[i][j] = rgb[i * imgSize + j];
            }
        }

        List<Callable<int[][]>> tasks = computeDirectly(square, tasksCount);

        ExecutorService executorService1Thread = Executors.newFixedThreadPool(threadsCount);
        long start = System.currentTimeMillis();
        List<Future<int[][]>> futures = executorService1Thread.invokeAll(tasks);
        res = getResult(futures, vertical);
        executorService1Thread.shutdown();
        /*
        * здесь создаём ExecutorService, добавляем в него задачи, которые будут заполнять результирующий массив
        * т.е каждая задача берёт на себя расчёт определённого количества пикселей(partSize) результирующего массива
        * расчёт значений пикселей выполнять в методе computeDirectly
        * ждём пока все задачи отработают и закрываем ExecutorService
        * */
        return res;
    }

    public static List<Callable<int[][]>> computeDirectly(int[][] square, int threadsCount) {
        /*
        * расчёт пикселей начиная с индекса start до start + length
        * тут реализовать алгоритм расчёта среднего значения пикселя, исходя из ширины маски (это mBlurWidth)
        * после расчёта очередного пикселя, помещаем его в массив destination
        * */
        int vertical = square[0].length;
        final int partSize = vertical / threadsCount;
        final int[][] squareRezult = new int[imgSize][vertical];
        List<Callable<int[][]>> tasks = new ArrayList<>();

        for (int p = 0; p < threadsCount; p++) {

            //результирующую матрицу наполняю нулями
            for (int i = 0; i < square.length; i++) {
                for (int j = 0; j < vertical; j++) {
                    squareRezult[i][j] = 0;
                }
            }

            final int finalP = p;

            Callable<int[][]> callable = new Callable<int[][]>() {

                @Override
                public int[][] call() throws Exception {

                    int finishLine = 0;
                    int startLine = 0;

                    if (finalP == 0) startLine = 0;
                    else startLine = (finalP) * partSize;

                    if (finalP == (threadsCount - 1)) finishLine = vertical;
                    else finishLine = (finalP + 1) * partSize;

                    //рассчитываю матрицу для размытия
                    int half = mBlurWidth / 2;
                    for (int i = 0; i < imgSize; i++) {
                        for (int j = startLine; j < finishLine; j++) {

                            int minHorizont = max(0, i - half);
                            int maxHorizont = min(imgSize, i + half);
                            int lenghtHorizont = maxHorizont - minHorizont;
                            int minVert = max(0, j - half);
                            int maxVert = min(imgSize, j + half);
                            int lenghtVert = maxVert - minVert;

                            int[][] blur = new int[lenghtHorizont][lenghtVert];

                            for (int k = 0; k < lenghtHorizont; k++) {
                                for (int l = 0; l < lenghtVert; l++) {
                                    blur[k][l] = square[minHorizont + k][minVert + l];
                                }
                            }
                            //размытие пикселя
                            //squareRezult[i][j] = myBlur(blur, lenghtHorizont, lenghtVert);

                            int resultRed = 0;
                            int resultBlue = 0;
                            int resultGreen = 0;
                            int arraySize = lenghtHorizont * lenghtVert;

                            for (int bl_H = 0; bl_H < lenghtHorizont; bl_H++) {
                                for (int bl_V = 0; bl_V < lenghtVert; bl_V++) {

                                    resultGreen = resultGreen + ((blur[bl_H][bl_V] & 0x00_00_FF_00) >> 8);
                                    resultRed = resultRed + ((blur[bl_H][bl_V] & 0x00_FF_00_00) >> 16);
                                    resultBlue = resultBlue + (blur[bl_H][bl_V] & 0x00_00_00_FF);
                                }
                            }
                            squareRezult[i][j] = 0xFF_00_00_00 | (resultBlue / arraySize) | ((resultGreen / arraySize) << 8) | ((resultRed / arraySize) << 16);
                        }
                    }
                    return squareRezult;
                }

            };

            tasks.add(callable);
        }

        return tasks;
    }

    public static int myBlur(int[][] squareSource, int width, int high) {

        int result = 0;
        int resultRed = 0;
        int resultBlue = 0;
        int resultGreen = 0;
        int arraySize = width * high;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < high; j++) {

                resultGreen = resultGreen + ((squareSource[i][j] & 0x00_00_FF_00) >> 8);
                resultRed = resultRed + ((squareSource[i][j] & 0x00_FF_00_00) >> 16);
                resultBlue = resultBlue + (squareSource[i][j] & 0x00_00_00_FF);
            }
        }

        return 0xFF_00_00_00 | (resultBlue / arraySize) | ((resultGreen / arraySize) << 8) | ((resultRed / arraySize) << 16);
    }

    public static int[] getResult(List<Future<int[][]>> futures, int vertical) {
        try {
            int[][] result = new int[imgSize][vertical];
            int[] res = new int[imgSize * vertical];

//            for (int k = 0; k < futures.size(); k++) {
            for (int k = 0; k < 1; k++) {
                Future<int[][]> f = futures.get(k);

                int[][] part = f.get();

                for (int i = 0; i < imgSize; i++) {
                    for (int j = 0; j < vertical; j++) {
                        result[i][j] = result[i][j] + part[i][j];
                    }
                }
            }
            //собрал в линейный массив
            for (int i = 0; i < imgSize; i++) {
                for (int j = 0; j < vertical; j++) {
                    res[i * imgSize + j] = result[i][j];
                }
            }
            return res;
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static int[] imgToRgb(BufferedImage img) {
        int[] res = new int[imgSize * imgSize];
        img.getRGB(0, 0, imgSize, imgSize, res, 0, imgSize);
        return res;
    }

    private static BufferedImage rgbToImg(int[] rgb) {
        BufferedImage res = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_RGB);
        res.setRGB(0, 0, imgSize, imgSize, rgb, 0, imgSize);
        return res;
    }
}